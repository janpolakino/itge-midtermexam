//
//  Temperature.h
//  
//
//  Created by iOSDev on 9/6/14.
//
//

#import <Foundation/Foundation.h>

@interface Temperature : NSObject

{
    double celsius;
}

-(id) initWithFahrenheit:(double) f;
-(id) initWithCelsius:(double) c;

@end

