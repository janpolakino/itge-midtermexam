//
//  Temperature.m
//  
//
//  Created by iOSDev on 9/6/14.
//
//

#import "Temperature.h"

@implementation Temperature

-(id) initWithFahrenheit:(double) f
{
    self = [super init];
    if (!self) return nil;
    celsius = (f-32)*(5/9);
    return self;
    
}

-(id) initWithCelsius:(double) c
{
    self = [super init];
    if (!self) return nil;
    celsius = c;
    return self ;
}

-(double) fahrenheitValue
{
    return (celsius*((9/5))+32);

}

-(double) celsiusValue
{
    return celsius;
}

@end
