//
//  main.m
//  ITGEMidtermExam
//
//  Created by iOSDev on 9/6/14.
//  Copyright (c) 2014 University of the East. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "_sAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([_sAppDelegate class]));
    }
}
