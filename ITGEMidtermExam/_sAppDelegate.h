//
//  _sAppDelegate.h
//  ITGEMidtermExam
//
//  Created by iOSDev on 9/6/14.
//  Copyright (c) 2014 University of the East. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _sAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
